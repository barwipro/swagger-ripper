import React from 'react';
import Header from './components/Header.jsx';
import Navigation from './components/Navigation.jsx';

const styles = {
  container: {
    display: 'flex',
    flex:1,
    flexDirection: 'column',
    minHeight: '100%'
  },
  main: {
    flex: 1,
    display: 'flex',
    flexDirection: 'row'
  }
};

function App(props) {
  return (
    <div style={styles.container}>
      <Header {...props}/>
      <div style={styles.main}>
        <Navigation {...props}/>
        <div id="swagger" style={{flex: 1, display: 'flex', padding: 8}}>
          <div className="swagger-section">
            <div id="swagger-ui-container" className="swagger-ui-wrap"></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;