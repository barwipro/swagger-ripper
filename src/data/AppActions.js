import AppDispatcher from './AppDispatcher';
import AppActionTypes from './AppActionTypes'

const Actions = {
  addApi(data) {
    AppDispatcher.dispatch({
      type: AppActionTypes.ADD_TODO,
      data: data
    });
  },
  refresh(){
    AppDispatcher.dispatch({
      type: AppActionTypes.REFRESH
    });
  },

  createEnvironment(environment){
    AppDispatcher.dispatch({
      type: AppActionTypes.CREATE_ENVIRONMENT,
      environment: environment
    });
  },

  environmentSelected(environment){
    AppDispatcher.dispatch({
      type: AppActionTypes.ENVIRONMENT_SELECTED,
      environment: environment
    });
  },

  createSwagger(swaggerUrl){
    AppDispatcher.dispatch({
      type: AppActionTypes.CREATE_API,
      swaggerUrl:swaggerUrl
    })
  },
  
  createSwaggerFromFile(file){
    AppDispatcher.dispatch({
      type: AppActionTypes.CREATE_API_FROM_FILE,
      file:file
    })
  }
};

export default Actions;
