import {ReduceStore} from 'flux/utils';
import AppActionTypes from './AppActionTypes';
import AppDispatcher from './AppDispatcher';
import EndpointConfig from '../config/EndpointConfig.js'
import Immutable from 'immutable'

class EnvironmentStore extends ReduceStore {
  constructor() {
    super(AppDispatcher);
    this.reloadData()
  }

  addEnvironment(environmentUrl){
    const xhr = new XMLHttpRequest();
    xhr.open('post', EndpointConfig.environmentsEndpoint(), true);
    xhr.setRequestHeader('Content-type','text/plain');
    xhr.onloadend = ()=>{
        
    }
    xhr.send(environmentUrl);
  }

  reloadData(){
    const xhr = new XMLHttpRequest();
    xhr.open('get', EndpointConfig.environmentsEndpoint(), true);
    xhr.responseType = 'json'
    xhr.onreadystatechange = ((event)=>{
      if(xhr.readyState === XMLHttpRequest.DONE)
      {
          var data = event.target.response
          data.map((item)=> {
              AppDispatcher.dispatch({
                type: AppActionTypes.ADD_ENVIRONMENT,
                environment:item
              })
          });
      }
    }).bind(this)
    xhr.send()
  }

  getInitialState() {
    return Immutable.Map({environments:Immutable.List(['default']), selected:'default'});
  }

  reduce(state, action) {
    switch (action.type) {
      case AppActionTypes.ADD_ENVIRONMENT:
        return state.set('environments', state.get('environments').push(action.environment));
    case AppActionTypes.CREATE_ENVIRONMENT:
        this.addEnvironment(action.environment)
        return state.set('environments', state.get('environments').push(action.environment));
    case AppActionTypes.ENVIRONMENT_SELECTED:
        return state.set('selected', action.environment);
      default:
        return state;
    }
  }
}

export default new EnvironmentStore();