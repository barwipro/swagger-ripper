/**
 * Copyright (c) 2014-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

'use strict';

const ActionTypes = {
  ADD_API: 'ADD_API',
  CREATE_API_FROM_FILE: 'CREATE_API_FROM_FILE',
  RESET_API: 'RESET_API',
  ADD_ENVIRONMENT: 'ADD_ENVIRONMENT',
  CREATE_ENVIRONMENT: 'CREATE_ENVIRONMENT',
  ENVIRONMENT_SELECTED: 'ENVIRONMENT_SELECTED',
  REFRESH: 'REFRESH'
};

export default ActionTypes;
