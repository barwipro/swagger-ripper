import {ReduceStore} from 'flux/utils';
import AppActionTypes from './AppActionTypes';
import AppDispatcher from './AppDispatcher';
import EndpointConfig from '../config/EndpointConfig.js'
import yaml from 'js-yaml';
import Immutable from 'immutable'

class ApiStore extends ReduceStore {
  constructor() {
    super(AppDispatcher);
    this.reloadData()
  }

  createSwagger(swaggerUrl){
    const xhr = new XMLHttpRequest();
    xhr.open('post', EndpointConfig.apiEndpoint(), true);
    xhr.setRequestHeader('Content-type','text/plain');
    xhr.onloadend = (()=>{
      this.reloadData()
    }).bind(this)
    xhr.send(swaggerUrl);
  }

  createSwaggerFromFile(file){
    const xhr = new XMLHttpRequest();
    const formData = new FormData(file);

    xhr.open('post', EndpointConfig.apiEndpoint(), true);
    xhr.onloadend = ()=>{
      this.reloadData();
    }
    xhr.send(formData);
  }

  reloadData(){
    const xhr = new XMLHttpRequest();
    xhr.open('get', EndpointConfig.apiEndpoint(), true);
    xhr.responseType = 'json'
    xhr.onreadystatechange = ((event)=>{
      if(xhr.readyState === XMLHttpRequest.DONE)
      {
          this.state = Immutable.List([])
          AppDispatcher.dispatch({
              type: AppActionTypes.RESET_API,
              data:doc
            })
          var data = event.target.response
          for (var i = 0; i < data.length; i++) {
              var doc = yaml.safeLoad(data[i].swagger)
              doc.raw = data[i].swagger
              doc.url = EndpointConfig.apiEndpoint() + data[i].id
              AppDispatcher.dispatch({
                type: AppActionTypes.ADD_API,
                data:doc
              })
          }
      }
    }).bind(this)
    xhr.send()
  }

  getInitialState() {
    return Immutable.List([]);
  }

  reduce(state, action) {
    switch (action.type) {
      case AppActionTypes.ADD_API:
        return state.push(action.data);
      case AppActionTypes.CREATE_API:
        this.createSwagger(action.swaggerUrl);
        return state
      case AppActionTypes.RESET_API:
        return Immutable.List([])
      case AppActionTypes.CREATE_API_FROM_FILE:
        this.createSwaggerFromFile(action.file);
        return state
      default:
        return state;
    }
  }
}

export default new ApiStore();