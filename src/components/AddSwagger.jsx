import React, { Component } from 'react';
import xml2js from 'xml2js';
import {
   Button
   , ListGroupItem
   , ListGroup
   , FormControl
   , Modal
 } from 'react-bootstrap';

class AddSwagger extends Component {
  componentWillMount(){
    this.state = {
      search:'',
      apis:[]
    }
  }

  render(){
    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
          <Modal.Header closeButton>
            <Modal.Title>Add api</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <div style={{flex:1,flexDirection:'row', display:'flex', paddingBottom:10}}>
                <FormControl placeholder='Url to jar/json/yaml' style={{width:265}} type='text'
                  onChange={(event)=>{
                    this.setState({uploadSwaggerUrl:event.target.value})
                  }}/>
                <Button bsSize='small' bsStyle='primary'  onClick={this.handleUploadSwaggerUrl.bind(this)}>Add Swagger url</Button>
              </div>
              <form id='file_uploader' style={{flex:1,flexDirection:'row', display:'flex'}}>
                  <FormControl type='file' name='file' id='fileUploadControl' accept='.jar'/>
                  <Button bsStyle='primary' onClick={this.handleUplloadFile.bind(this)}>Upload</Button>
              </form>
              <div style={{flex:1,flexDirection:'row', display:'flex'}}>
                <FormControl placeholder='search' type='text' onChange={this.handleSearchChange.bind(this)} />
                <Button bsSize='small' bsStyle='primary'  onClick={this.handleSearch.bind(this)}>Search nexus</Button>
              </div>
              <ListGroup style={{flex:1}}>
                  {this.state.apis.map(function(listValue, index){
                    var style = listValue == this.state.selectedApi ? 'success' : 'info'
                    return <ListGroupItem key={index} bsStyle={style}>
                              <Button onClick={this.handleAddNexusApi.bind(this, listValue)}>Add</Button>
                              <a href='#'>{listValue.artifactId}</a>
                              </ListGroupItem>;
                  }.bind(this))}
                </ListGroup>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.onClose}>Close</Button>
          </Modal.Footer>
        </Modal>
    )
  }

  handleSearch(){
     const xhr = new XMLHttpRequest();
      xhr.open('get', 'http://nexus.sandbox.extranet.group/nexus/service/local/lucene/search?q=' + this.state.search, true);
      xhr.onreadystatechange = ((event)=>{
        if(xhr.readyState === XMLHttpRequest.DONE)
        {
            var parser = new xml2js.Parser({explicitArray:false, rootName: 'data', explicitRoot:false});
            parser.parseString(event.target.response, function (err, result) {
                var data = result.data.artifact
                this.setState({apis:data})
            }.bind(this));
        }
      }).bind(this)
      xhr.send()
  }

  handleAddNexusApi(data){
    var url = 'http://nexus.sandbox.extranet.group/nexus/service/local/artifact/maven/resolve?'
    url += 'r=' + data.latestSnapshotRepositoryId;
    url += '&g=' + data.groupId
    url += '&a=' + data.artifactId
    url += '&v=' + data.latestSnapshot
    url += '&c=&e=jar&isLocal=true'
    
    const xhr = new XMLHttpRequest();
      xhr.open('get', url, true);
      xhr.onreadystatechange = ((event)=>{
        if(xhr.readyState === XMLHttpRequest.DONE)
        {
            var parser = new xml2js.Parser({explicitArray:false, explicitRoot:false});
            parser.parseString(event.target.response, function (err, result) {
                var url = 'http://nexus.sandbox.extranet.group/nexus/service/local/repositories/maven-repo-snapshot/content' + result.data.repositoryPath
                this.uploadSwaggerUrl(url)
            }.bind(this));
        }
      }).bind(this)
      xhr.send()
  }

  handleSearchChange(event) {
    this.setState({search: event.target.value})
  }

  handleFileSelected(event) {
    this.setState({file: event.target.files[0]})
  }

  handleUplloadFile() {
    this.props.onCreateSwaggerFromFile($('#file_uploader')[0])
    this.props.onClose()
  }
  
  handleUploadSwaggerUrl(){
    var url = this.state.uploadSwaggerUrl;
    this.uploadSwaggerUrl(url)
  }

  uploadSwaggerUrl(url){
    if(url.startsWith('http://') == false && url.startsWith('https://') == false){
      url = 'http://' + url;
    }
    this.props.onCreateSwagger(url)
    this.props.onClose()
  }
}

export default AddSwagger;