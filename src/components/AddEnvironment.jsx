import React, { Component } from 'react';
import {
   Button
   , FormControl
   , Modal
 } from 'react-bootstrap';

class AddEnvironment extends Component {
  
  render(){
    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
          <Modal.Header closeButton>
            <Modal.Title>Add environment</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <div style={{flex:1,flexDirection:'row', display:'flex', paddingBottom:10}}>
                <FormControl placeholder='environment' style={{width:265}} type='text'
                  onChange={(event)=>{
                    this.setState({environmentUrl:event.target.value})
                  }}/>
                <Button bsStyle='primary'  onClick={this.handleAddEnvironment.bind(this)}>Add Environment</Button>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.props.onClose}>Close</Button>
          </Modal.Footer>
        </Modal>
    )
  }

  handleAddEnvironment(){
    this.props.onCreateEnvironment(this.state.environmentUrl)
    this.props.onClose()
  }

}


export default AddEnvironment;