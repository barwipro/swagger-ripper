import React, { Component } from 'react';
import yaml from 'js-yaml';
import ApiStore from '../data/ApiStore';
import EnvironmentStore from '../data/EnvironmentStore';
import {
   ListGroupItem
   , ListGroup
   , FormControl
 } from 'react-bootstrap';


class Navigation extends Component {
  componentWillMount(){
    this.state = {
      search:'',
      apis:[],
      environment: EnvironmentStore.getState().get('selected'),
      selectedApi:null
    }
    ApiStore.addListener(this.onChange.bind(this))
    EnvironmentStore.addListener(this.onChange.bind(this))
  }

  onChange(){
    this.setState({apis: ApiStore.getState(), environment: EnvironmentStore.getState().get('selected')});
  }

  componentDidUpdate(){
    if(this.state.selectedApi == null && this.state.apis.size > 0){
      this.selectApi(this.state.apis.get(0))
    }
  }

  render(){
    this.refreshApi()
    return (
      <div style={{flexDirection: 'column', flex:0.2}}>
        <FormControl placeholder='search' type='text' onChange={this.handleChange.bind(this)} />
        <div>
          <ListGroup style={{flex:1}}>
            {this.state.apis.filter((listValue) => listValue.info.title.toLowerCase().includes(this.state.search.toLowerCase())).
              map(function(listValue, index){
              var style = listValue == this.state.selectedApi ? 'success' : 'info'
              return <ListGroupItem key={index} bsStyle={style}><a href='#' onClick={this.handleClick.bind(this, listValue)}>{listValue.info.title}</a></ListGroupItem>;
            }.bind(this))}
          </ListGroup>
        </div>
      </div>
    )
  }

  handleChange(event) {
    this.setState({search: event.target.value})
  }

  handleClick(data){
    this.selectApi(data)
  }

  selectApi(data){
    this.setState({selectedApi:data})
  }

  renderApi(data){
    var url = data.host != null ? data.host : '';
    var dta = yaml.safeLoad(data.raw)
    if(this.state.environment != 'default'){
      url = this.state.environment
      dta.host = url
    }
    window.reloadSwaggerJson(dta, url)
  }

  refreshApi(){
    if(this.state.selectedApi){
      this.renderApi(this.state.selectedApi)
    }
  }

}

export default Navigation;