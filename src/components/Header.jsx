import React, { Component } from 'react';
import AddSwagger from './AddSwagger.jsx';
import AddEnvironment from './AddEnvironment.jsx';
import EnvironmentStore from '../data/EnvironmentStore.js'

import {
   Button
   , FormControl
   , ButtonGroup
 } from 'react-bootstrap';

const styles = {
  header: {
    display: 'flex',
    backgroundColor: '#1f1f1f',
    padding: 5,
    alignItems: 'center'
  },
  title: {
    fontSize: '2.2rem',
    margin: '1rem',
    color: 'white'
  },
  text: {
    color: 'white'
  }
};

class Header extends Component{
   
  componentWillMount(){
    this.state = {
      show:false,
      showEnvironment:false,
      environmentsState: EnvironmentStore.getState()
    }
    EnvironmentStore.addListener(this.onChange.bind(this))
  }

  onChange(){
    this.setState({environmentsState: EnvironmentStore.getState()});
  }

  render(){
    return (
    <div style={styles.header}>
      <AddSwagger {...this.props} show={this.state.show} onClose={this.onAddClose.bind(this)} onReload={this.props.onReload}/>
      <AddEnvironment {...this.props} show={this.state.showEnvironment} onClose={this.onAddClose.bind(this)} onReload={this.props.onReload}/>
      <h1 style={styles.title} >Swagger-ripper</h1>
      <div style={{flex:1}}/>
      <ButtonGroup style={{flexDirection:'row', paddingRight: 10}}>
        <Button title='Add environment' onClick={this.addEnvironmentSelected.bind(this)}>Add environment</Button>
        <Button title='Add Api' onClick={this.addApiSelected.bind(this)}>Add api</Button>
      </ButtonGroup>
      <div>
        <p1 style={styles.text}>Select Environment</p1>
        <FormControl componentClass='select' placeholder='select' onChange={this.handlEnvironmentSelected.bind(this)}>
            {this.state.environmentsState.get('environments').map(function(listValue, index){
              return <option value={listValue} key={index}>{listValue}</option>;
            }.bind(this))}
        </FormControl>
      </div>
    </div>
    )
  }

  addApiSelected(){
    this.props.onRefresh()
    this.setState({show:true});
  }

  addEnvironmentSelected(){
    this.setState({showEnvironment:true});
  }

  onAddClose(){
    this.setState({show:false, showEnvironment:false});
  }
  
  handlEnvironmentSelected(event){
    this.props.onEnvironmentSelected(event.target.value)
  }
  
}


export default Header;