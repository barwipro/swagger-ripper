// Application entrypoint.

// Render the top-level React component
import AppContainer from './containers/AppContainer';
import React from 'react';
import ReactDOM from 'react-dom';
//var SwaggerUi = require('swagger-ui')

ReactDOM.render(<AppContainer/>, document.getElementById('react-root'));

window.reloadSwaggerJson = function(json, url){
  var swaggerUi = new SwaggerUi({
    url:url,
    spec: json,
    dom_id: 'swagger-ui-container'
  });

  swaggerUi.load();
}