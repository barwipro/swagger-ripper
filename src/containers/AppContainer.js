import AppView from '../App.jsx';
import {Container} from 'flux/utils';
import AppActions from '../data/AppActions';
import ApiStore from '../data/ApiStore';
import EnvironmentStore from '../data/EnvironmentStore';

function getStores() {
  return [
    ApiStore,
    EnvironmentStore
  ];
}

function getState() {
  return {
    apis: ApiStore.getState(),
    environments: EnvironmentStore.getState(),

    onAdd: AppActions.addApi,
    onCreateEnvironment: AppActions.createEnvironment,
    onRefresh: AppActions.refresh,
    onEnvironmentSelected: AppActions.environmentSelected,
    onCreateSwagger: AppActions.createSwagger,
    onCreateSwaggerFromFile: AppActions.createSwaggerFromFile
    
  };
}

export default Container.createFunctional(AppView, getStores, getState);