//Config = require('Config')
let host = HOST;
let environmentsEndpoint = host + '/environment/';
let apiEndpoint = host + '/api/';

class EndpointConfig {

  static environmentsEndpoint(){
      return environmentsEndpoint;
  }

  static apiEndpoint(){
      return apiEndpoint;
  }
}

export default EndpointConfig;